package Primitiva;

public class Drum {

	private int drum[];
	private int currentNumbers;
	private int maxNumber;
	
	public Drum (int maxNumber) {
		this.maxNumber = maxNumber;
		currentNumbers = maxNumber;
		drum = new int[maxNumber];
		inicializeDrum();
		shuffle();
	}
	
	
	
	public int getCurrentNumbers() {
		return currentNumbers;
	}
	
	
	
	private void inicializeDrum() {
		for (int i = 0; i < maxNumber; i++) {
			drum[i] = i+1;
		}
	}
	
	private void shuffle() {
		for (int i = 0; i < 1000; i++) {
			int randomOri = (int)(Math.random()*maxNumber);
			int switchCard = drum[randomOri];
			int randomDest = (int)(Math.random()*maxNumber);
			
			drum[randomOri] = drum[randomDest];
			drum[randomDest] = switchCard;
		}
	}
	
	public int extractNumber() {
		currentNumbers--;
		int number = drum[currentNumbers];		
		
		return number;
	}	
}
