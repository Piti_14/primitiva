package Primitiva;

public class Ticket {
	
	private final int LENGHTBLOCKS = 6;
	private Block arrayBlocks[];
	private Drum drum;
	
	public Ticket (int numberOfBlocks, boolean full) {
		if (full) {
			drum = new Drum(Block.getBlockMaxlong());
		}
		arrayBlocks = new Block [numberOfBlocks];
		createBlocks(full);
		
	}
	
	
	
	@Override
	public String toString () {
		String string = "";
		
		for (int i = 0; i < arrayBlocks.length; i++) {
			string += arrayBlocks[i].toString();
			
			if (i != (arrayBlocks.length - 1)) {
				string += "\n";
			}
		}
		
		return string;
	}
	
	
	public String printUsedNumbers() {
		String string = "";
		int array[] = new int[50];
		
		for (int i = 0; i < arrayBlocks.length; i++) {
			for (int n = 0; n < arrayBlocks[i].getBlockNumbers().length; n++) {
				array[arrayBlocks[i].getBlockNumbers()[n]] = arrayBlocks[i].getBlockNumbers()[n];
			}
		}
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0) {
				if (string.length() == 0) {
				string += array[i];
				} else {
					string += ", " + array[i];
				}
			}
		}
		
		return string;
	}
	
	private void createBlocks(boolean full) {
		
		if (full) {
			for (int i = 0; i < arrayBlocks.length; i++) {
				arrayBlocks[i] = new Block(LENGHTBLOCKS, drum);
			}
		} else {
			for (int i = 0; i < arrayBlocks.length; i++) {
				arrayBlocks[i] = new Block(LENGHTBLOCKS);
			}
		}
	}
	
	public String remainingNumbers() {
		String s = "";
		int currentNumber = drum.getCurrentNumbers();
		
		for (int i = 0; i < currentNumber; i++) {
			if (i != 0) {
				s += ", " + drum.extractNumber();
			} else {
				s += drum.extractNumber();
			}
		}
		return s;
	}
}
