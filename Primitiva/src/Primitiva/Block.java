package Primitiva;

import java.util.Arrays;

public class Block {

	private int blockNumbers[];
	private static final int blockMaxLong = 49;
	
	
	
	public Block (int longNumber) {
		
		longNumber = checkInPermittedValors(longNumber);
		
		blockNumbers = createArrayOfNumbers(longNumber);
		
		Arrays.sort(blockNumbers);
	}
	
	public Block (int longNumber, Drum drum) {
		
		longNumber = checkInPermittedValors(longNumber);
		
		blockNumbers = createBlockNumbersWithDrum(longNumber,drum);
		
		Arrays.sort(blockNumbers);
	}
	
	public Block (int... preselectedNumber) {
		
		blockNumbers = preselectedNumber;
	}


	
	public int[] getBlockNumbers() {
		return blockNumbers;
	}	
	public static int getBlockMaxlong() {
		return blockMaxLong;
	}
	
	
	@Override
	public String toString() {
		String string = "";
		
		for (int i = 0; i < blockNumbers.length; i++) {
			if (i == 0) {
				string += blockNumbers[i];
			} else {
				string += ", " + blockNumbers[i];
			}
		}
		
		return string;
	}
	
	private int[] createArrayOfNumbers (int longNumber){
		int number[] = new int [longNumber];
		int allNumbers[] = new int [blockMaxLong];
		
		for (int i = 1; i <= blockMaxLong; i++) { //Inicializar array de números
			allNumbers[i-1] = i;
		}
		
		inicializeNumbers(number, allNumbers);

		return number;
	}
	
	private int[] createBlockNumbersWithDrum(int longNumber, Drum drum) {
		int number[] = new int [longNumber];
		
		for (int i = 0; i < longNumber; i++) {
			number[i] = drum.extractNumber();
		}

		return number;
	}

	private void inicializeNumbers(int[] number, int[] allNumbers) {
		
		for (int i = 0; i < number.length; i++) {
			int randomNumber = (int) ((Math.random()*allNumbers.length+1)-i);
			if (randomNumber < 0) {
				randomNumber = 0;
			}
			
			number[i] = allNumbers[randomNumber];
			
			allNumbers[randomNumber] = allNumbers[allNumbers.length-1-i];
			}
	}
	
	private int checkInPermittedValors(int longNumber) {
		if (longNumber < 6) {
			longNumber = 6;
		} else if (longNumber > blockMaxLong) {
			longNumber = blockMaxLong;
		}
		return longNumber;
	}
}
