package Primitiva;

public class MainPrimitiva {

	public static void main(String[] args) {
		Block block = new Block(5, 10, 15, 20, 25, 30, 35, 40, 45);
		//System.out.println(block.toString());
		
		Ticket ticket = new Ticket (7, true);
		System.out.println(ticket.toString());
		System.out.println(ticket.printUsedNumbers());
		System.out.println("Remainig numbers: "+ticket.remainingNumbers());
	}

}
